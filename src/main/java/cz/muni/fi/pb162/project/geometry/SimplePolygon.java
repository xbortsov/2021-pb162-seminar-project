package cz.muni.fi.pb162.project.geometry;

import cz.muni.fi.pb162.project.exception.MissingVerticesException;
import cz.muni.fi.pb162.project.utils.SimpleMath;

import java.util.Arrays;

/**
 * Class for working with Simple polygons
 *
 * @author Vitalii Bortsov
 */
abstract class SimplePolygon implements Polygon {
    /**
     * Checks an input array with vertices for validity.
     *
     * @param vertices array of vertices
     * @throws MissingVerticesException when the array contains less than three vertices.
     */
    protected SimplePolygon(Vertex2D[] vertices) {
        if(vertices == null) {
            throw new IllegalArgumentException("Vertices must not be null");
        }
        if(Arrays.asList(vertices).contains(null)) {
            throw new IllegalArgumentException("Vertices must not be null");
        }
        if(vertices.length < 3) {
            throw new MissingVerticesException("There must be at least 3 elements");
        }
    }

    /**
     * Method returns the difference between the largest and smallest Y coordinates in an polygon
     *
     * @return height of the polygon
     */
    @Override
    public double getHeight() {
        return SimpleMath.maxY(this) - SimpleMath.minY(this);
    }

    /**
     * Method returns the difference between the largest and smallest X coordinates in an polygon
     *
     * @return width of the polygon
     */
    @Override
    public double getWidth() {
        return SimpleMath.maxX(this) - SimpleMath.minX(this);
    }

    @Override
    public Vertex2D[] getAllVertices() {
        Vertex2D[] vertices = new Vertex2D[this.getNumVertices()];
        for (int i = 0; i < this.getNumVertices(); i++) {
            vertices[i] = this.getVertex(i);
        }
        return vertices;
    }

    @Override
    public String toString() {
        final int verticesNum = this.getNumVertices();
        StringBuilder string = new StringBuilder("Polygon: vertices =");
        for (int i = 0; i < verticesNum; i++) {
            string.append(" ").append(this.getVertex(i));
        }
        return string.toString();
    }
}
