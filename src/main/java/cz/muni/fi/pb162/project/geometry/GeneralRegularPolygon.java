package cz.muni.fi.pb162.project.geometry;

/**
 * This class serves as a common superclass for all regular polygons.
 *
 * @author Vitalii Bortsov
 */
public class GeneralRegularPolygon implements RegularPolygon, Colored {
    private final Vertex2D center;
    private final int numEdges;
    private final double radius;
    private Color color = Color.BLACK;

    /**
     * Constructor of regular polygon
     *
     * @param center coordinates of the center
     * @param numEdges number of edges
     * @param radius length of radius. If it is not positive, it will be set on 1.0
     */
    GeneralRegularPolygon(Vertex2D center, int numEdges, double radius) {
        this.center = center;
        this.numEdges = numEdges;
        if (radius < 0) {
            this.radius = 1.0;
        } else {
            this.radius = radius;
        }
    }

    @Override
    public Vertex2D getCenter() {
        return center;
    }

    @Override
    public double getRadius() {
        return radius;
    }

    @Override
    public double getWidth() {
        return 2 * radius;
    }

    @Override
    public double getHeight() {
        return 2 * radius;
    }

    @Override
    public int getNumEdges() {
        return numEdges;
    }

    @Override
    public double getEdgeLength() {
        return 2 * radius * Math.sin(Math.PI / numEdges);
    }

    @Override
    public Vertex2D getVertex(int index) {
        return new Vertex2D(center.getX() - (radius * Math.cos(index * 2 * Math.PI / numEdges)),
                center.getY() - (radius * Math.sin(index * 2 * Math.PI / numEdges)));
    }

    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public void setColor(Color newColor) {
        this.color = newColor;
    }

    @Override
    public String toString() {
        return numEdges + "-gon: center=" + center + ", radius=" + radius + ", color=" + color;
    }
}
