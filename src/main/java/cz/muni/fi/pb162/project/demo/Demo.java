package cz.muni.fi.pb162.project.demo;

import cz.muni.fi.pb162.project.geometry.RegularOctagon;
import cz.muni.fi.pb162.project.geometry.Vertex2D;

/**
 * Class for running main method.
 *
 * @author Vitalii Bortsov
 */
public class Demo {
    /**
     *
     * @param args nothing
     */
    public static void main(String[] args) {
        RegularOctagon testOctagon = new RegularOctagon(new Vertex2D(0, 0), 1.0);

        System.out.println(testOctagon);
    }
}
