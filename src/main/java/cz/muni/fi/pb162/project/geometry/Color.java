package cz.muni.fi.pb162.project.geometry;

/**
 * Enum class with colors
 *
 * @author Vitalii bortsov
 */
public enum Color {
    WHITE,
    BLACK,
    RED,
    GREEN,
    BLUE,
    ORANGE,
    YELLOW;

    @Override
    public String toString() {
        return name().toLowerCase();
    }
}
