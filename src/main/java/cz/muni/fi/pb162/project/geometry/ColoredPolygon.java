package cz.muni.fi.pb162.project.geometry;

/**
 * Class for work with colored polygons
 *
 * @author Vitalii Bortsov
 */
public class ColoredPolygon implements Polygon{
    private final Polygon polygon;
    private final Color color;

    /**
     * Constructor for colored polygon
     *
     * @param polygon polygon to be colored
     * @param color color for the polygon
     */
    public ColoredPolygon(Polygon polygon, Color color) {
        this.polygon = polygon;
        this.color = color;
    }

    /**
     * Getter for color
     * @return color
     */
    public Color getColor() {
        return color;
    }

    /**
     * Getter for polygon
     * @return polygon
     */
    public Polygon getPolygon() {
        return polygon;
    }

    @Override
    public boolean equals(Object object) {
        if(object == null) {
            return false;
        }
        if(this == object) {
            return true;
        }
        if(this.getClass() != object.getClass()) {
            return false;
        }

        ColoredPolygon secondPolygon = (ColoredPolygon) object;
        return (this.color == secondPolygon.color) && (this.polygon.equals(secondPolygon.polygon));
    }

    @Override
    public int hashCode() {
        return 31 * this.color.hashCode() + this.polygon.hashCode();
    }

    @Override
    public double getWidth() {
        return polygon.getWidth();
    }

    @Override
    public double getHeight() {
        return polygon.getHeight();
    }

    @Override
    public Vertex2D getVertex(int index) {
        return polygon.getVertex(index);
    }

    @Override
    public int getNumVertices() {
        return polygon.getNumVertices();
    }

    @Override
    public Vertex2D[] getAllVertices() {
        Vertex2D[] vertices = new Vertex2D[polygon.getNumVertices()];
        for (int i = 0; i < polygon.getNumVertices(); i++) {
            vertices[i] = polygon.getVertex(i);
        }
        return vertices;
    }
}
