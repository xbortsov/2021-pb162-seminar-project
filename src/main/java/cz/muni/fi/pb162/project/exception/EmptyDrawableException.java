package cz.muni.fi.pb162.project.exception;

/**
 * Exception for empty drawable objects
 * @author Vitalii Bortsov
 */
public class EmptyDrawableException extends Exception {
    /**
     * Exception with message
     * @param errorMessage message for an exception
     */
    public EmptyDrawableException(String errorMessage) {
        super(errorMessage);
    }

    /**
     * Exception with message and error
     * @param errorMessage message for an exception
     * @param err error
     */
    public EmptyDrawableException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }
}
