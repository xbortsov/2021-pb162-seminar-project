package cz.muni.fi.pb162.project.exception;

/**
 * Exception for objects with transparent color
 * @author Vitalii Bortsov
 */
public class TransparentColorException extends Exception {
    /**
     * Exception with message
     * @param errorMessage message for an exception
     */
    public TransparentColorException(String errorMessage) {
        super(errorMessage);
    }

    /**
     * Exception with message and error
     * @param errorMessage message for an exception
     * @param err error
     */
    public TransparentColorException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }
}
