package cz.muni.fi.pb162.project.geometry;

/**
 * Class for creating a snowman from different circular objects.
 *
 * @author Vitalii Bortsov
 */
public class Snowman {
    public static final int COUNT = 3;
    private final RegularPolygon[] balls = new RegularPolygon[COUNT];
    private static final double REDUCTION = 0.8;

    /**
     * Constructor to create a snowman from circular objects.
     *
     * @param sphere lower sphere of snowman
     * @param reductionFactor the upper parts of the snowman will shrink by this factor.
     *                        Must be in range (0..1>. If is not in this range,
     *                        the upper parts of the snowman will shrink by 0.8
     */
    public Snowman(RegularPolygon sphere, double reductionFactor) {
        RegularPolygon currentBall = sphere;
        Vertex2D currentCenter = sphere.getCenter();
        double reduction = reductionFactor;
        double currentRadius = sphere.getRadius();

        if(reductionFactor <= 0 || reductionFactor > 1) {
            reduction = REDUCTION;
        }
        balls[0] = sphere;
        for(int i = 1; i < COUNT; i++) {
            currentRadius = currentRadius * reduction;
            currentCenter = new Vertex2D(currentCenter.getX(),
                    currentCenter.getY() + currentBall.getRadius() + currentRadius);
            currentBall = new GeneralRegularPolygon(currentCenter, sphere.getNumEdges(), currentRadius);
            balls[i] = currentBall;
        }
    }

    /**
     * Method returns an array of all "show balls" from the lowest to the highest.
     *
     * @return an array of all "show balls".
     */
    public RegularPolygon[] getBalls() {
        return balls;
    }
}
