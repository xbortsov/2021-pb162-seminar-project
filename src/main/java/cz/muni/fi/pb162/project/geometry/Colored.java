package cz.muni.fi.pb162.project.geometry;

/**
 * Interface for objects that can be colored.
 *
 * @author Vitalii Bortsov
 */
public interface Colored {
    /**
     * Method returns the color of the object
     *
     * @return color of the colored object
     */
    Color getColor();

    /**
     * Method for setting the color of object
     *
     * @param newColor color to set
     */
    void setColor(Color newColor);
}
