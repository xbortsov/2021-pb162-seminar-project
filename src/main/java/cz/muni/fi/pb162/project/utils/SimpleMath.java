package cz.muni.fi.pb162.project.utils;
import cz.muni.fi.pb162.project.geometry.Polygon;

/**
 * Class with simple geometry methods.
 *
 * @author Vitalii Bortsov
 */
public class SimpleMath {
    /**
     * Method that finds minimum x coordinate in n-gon.
     *
     * @param polygon Polygon object. Must not be null.
     * @return minimum X coordinate.
     */
    public static double minX(Polygon polygon) {
        final int verticesNum = polygon.getNumVertices();
        double xCoord = polygon.getVertex(0).getX();
        double minX = xCoord;
        for(int i = 1; i < verticesNum; i++) {
            xCoord = polygon.getVertex(i).getX();
            if(xCoord < minX) {
                minX = xCoord;
            }
        }
        return minX;
    }

    /**
     * Method that finds minimum y coordinate in n-gon.
     *
     * @param polygon Polygon object. Must not be null.
     * @return minimum Y coordinate.
     */
    public static double minY(Polygon polygon) {
        final int verticesNum = polygon.getNumVertices();
        double yCoord = polygon.getVertex(0).getY();
        double minY = yCoord;
        for(int i = 1; i < verticesNum; i++) {
            yCoord = polygon.getVertex(i).getY();
            if(yCoord < minY) {
                minY = yCoord;
            }
        }
        return minY;
    }

    /**
     * Method that finds maximum x coordinate in n-gon.
     *
     * @param polygon Polygon object. Must not be null.
     * @return maximum X coordinate.
     */
    public static double maxX(Polygon polygon) {
        final int verticesNum = polygon.getNumVertices();
        double xCoord = polygon.getVertex(0).getX();
        double maxX = xCoord;
        for(int i = 1; i < verticesNum; i++) {
            xCoord = polygon.getVertex(i).getX();
            if(xCoord > maxX) {
                maxX = xCoord;
            }
        }
        return maxX;
    }

    /**
     * Method that finds maximum y coordinate in n-gon.
     *
     * @param polygon Polygon object. Must not be null.
     * @return maximum Y coordinate.
     */
    public static double maxY(Polygon polygon) {
        final int verticesNum = polygon.getNumVertices();
        double yCoord = polygon.getVertex(0).getY();
        double maxY = yCoord;
        for(int i = 1; i < verticesNum; i++) {
            yCoord = polygon.getVertex(i).getY();
            if(yCoord > maxY) {
                maxY = yCoord;
            }
        }
        return maxY;
    }
}
