package cz.muni.fi.pb162.project.geometry;

/**
 * Class for creating octagon.
 *
 * @author Vitalii Bortsov
 */
public class RegularOctagon extends GeneralRegularPolygon {

    /**
     *
     * @param center center coordinates
     * @param radius length of radius. If it is not positive, it will be set on 1.0
     */
    public RegularOctagon(Vertex2D center, double radius) {
        super(center, 8, radius);
    }
}
