package cz.muni.fi.pb162.project.geometry;

import cz.muni.fi.pb162.project.exception.MissingVerticesException;
import cz.muni.fi.pb162.project.utils.SimpleMath;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Similar class to the ArrayPolygon class.
 * The vertices of the polygon will be stored in a suitable collection.
 *
 * @author Vitalii Bortsov
 */
public class CollectionPolygon extends SimplePolygon {
    private List<Vertex2D> listOfVertices;

    /**
     * Constructor for list of vertices of this polygon
     *
     * @param vertices array of vertices of the polygon
     */
    public CollectionPolygon(Vertex2D[] vertices) {
        super(vertices);
        listOfVertices = new ArrayList<>(Arrays.asList(vertices));
    }

    /**
     * Constructor for list of vertices of this polygon
     *
     * @param vertices list of vertices of the polygon
     */
    public CollectionPolygon(List<Vertex2D> vertices) {
        super(vertices.toArray(new Vertex2D[0]));
        listOfVertices = new ArrayList<>(vertices);
    }

    @Override
    public Vertex2D getVertex(int index) {
        if(index < 0) {
            throw new IllegalArgumentException("index must be positive");
        }

        return listOfVertices.get(index % listOfVertices.size());
    }

    @Override
    public int getNumVertices() {
        return listOfVertices.size();
    }

    @Override
    public Vertex2D[] getAllVertices() {
        return (Vertex2D[]) listOfVertices.toArray();
    }

    @Override
    public boolean equals(Object object) {
        if(object == null) {
            return false;
        }
        if(object == this) {
            return true;
        }
        if(this.getClass() != object.getClass()) {
            return false;
        }

        CollectionPolygon secondPolygon = (CollectionPolygon) object;
        if (this.listOfVertices.size() != secondPolygon.listOfVertices.size()) {
            return false;
        }
        return listOfVertices.equals(secondPolygon.listOfVertices);
    }

    @Override
    public int hashCode() {
        return listOfVertices.hashCode();
    }

    /**
     * Method returns polygon without the leftmost vertices
     *
     * @return a new polygon without the leftmost vertices
     * If the new polygon no longer contains any vertices after removing the leftmost vertices,
     * the method returns null
     */
    public CollectionPolygon withoutLeftmostVertices() {
        if(listOfVertices.size() == 3) {
            throw new MissingVerticesException("Must be more than 3 vertices after this method");
        }
        CollectionPolygon withoutLeft = new CollectionPolygon(listOfVertices);
        double min = SimpleMath.minX(withoutLeft);
        withoutLeft.listOfVertices = withoutLeft.listOfVertices
                .stream()
                .filter(v -> v.getX() != min)
                .collect(Collectors.toList());
        if(withoutLeft.listOfVertices.size() == 0) {
            return null;
        }
        return withoutLeft;
    }
}
