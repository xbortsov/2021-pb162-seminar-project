package cz.muni.fi.pb162.project.geometry;

/**
 * Class for creating a circle
 * @author Vitalii Bortsov
 */
public class Circle extends GeneralRegularPolygon implements Measurable, Circular {
    /**
     * Simple constructor of circle-object
     * @param center coordinates of center in Vertex2D
     * @param radius length of radius. If it is not positive, it will be set on 1.0
     */
    public Circle(Vertex2D center, double radius) {
        super(center, Integer.MAX_VALUE, radius);
        setColor(Color.RED);
    }

    /**
     * free-parametric constructor. Default values are center [0, 0] and radius 1.0
     */
    public Circle() {
        this(new Vertex2D(0,0), 1.0);
    }

    @Override
    public double getEdgeLength() {
        return 0;
    }

    @Override
    public String toString() {
        return "Circle: center=" + getCenter() + ", radius=" + getRadius();
    }
}
