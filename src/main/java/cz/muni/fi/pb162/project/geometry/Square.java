package cz.muni.fi.pb162.project.geometry;

/**
 * Class for creating a square object.
 *
 * @author Vitalii Bortsov
 */
public class Square extends GeneralRegularPolygon implements Circular {

    /**
     * Constructor for creating a square object.
     *
     * @param center coordinates of the center.
     * @param diameter diameter of the circumscribed circle.
     */
    public Square(Vertex2D center, double diameter) {
        super(center, 4, diameter / 2);
    }

    /**
     * Constructor for creating a square object.
     *
     * @param circular circumscribed circle
     */
    public Square(Circular circular) {
        this(circular.getCenter(), circular.getRadius() * 2);
    }

    @Override
    public String toString() {
        return "Square: vertices=" +
                getVertex(0) + " " + getVertex(1) +
                " " + getVertex(2) + " " + getVertex(3);
    }
}
