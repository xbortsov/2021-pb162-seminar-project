package cz.muni.fi.pb162.project.geometry;

import java.util.Arrays;

/**
 * Class for working with vertices of polygons
 *
 * @author Vitalii Bortsov
 */
public class ArrayPolygon extends SimplePolygon {
    private final Vertex2D[] arrayOfVertices;

    /**
     * Constructor of the array with vertices of the polygon
     *
     * @param vertices coordinates of the vertices of the polygon;
     *                        must not be null,
     *                        must not contain null-element,
     *                        must has at least 3 elements.
     */
    public ArrayPolygon(Vertex2D[] vertices) {
        super(vertices);
        arrayOfVertices = Arrays.copyOf(vertices, vertices.length);
    }

    @Override
    public Vertex2D getVertex(int index) {
        if(index < 0) {
            throw new IllegalArgumentException("index must be positive");
        }

        return arrayOfVertices[index % arrayOfVertices.length];
    }

    @Override
    public int getNumVertices() {
        return arrayOfVertices.length;
    }

    @Override
    public Vertex2D[] getAllVertices() {
        return arrayOfVertices;
    }

    @Override
    public boolean equals(Object object) {
        if(object == null) {
            return false;
        }
        if(this.getClass() != object.getClass()) {
            return false;
        }

        ArrayPolygon secondArray = (ArrayPolygon) object;
        if (this.arrayOfVertices.length != secondArray.arrayOfVertices.length) {
            return false;
        }
        return Arrays.equals(this.arrayOfVertices, secondArray.arrayOfVertices);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(arrayOfVertices);
    }
}
