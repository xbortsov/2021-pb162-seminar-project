package cz.muni.fi.pb162.project.utils;

import cz.muni.fi.pb162.project.geometry.Measurable;
import cz.muni.fi.pb162.project.geometry.Triangle;

/**
 * Class, which allows you to "measure" objects.
 *
 * @author Vitalii Bortsov
 */
public class Gauger {

    /**
     * Method prints triangle information and its width with height.
     *
     * @param triangle the triangle that you want to get information about.
     */
    public static void printMeasurement(Triangle triangle) {
        System.out.println("" + triangle);
        Gauger.printMeasurement((Measurable) triangle);
    }

    /**
     * Method width with height of any measurable object.
     *
     * @param measurable the measurable object that you want to get information about.
     */
    public static void printMeasurement(Measurable measurable) {
        System.out.println("Width: " + measurable.getWidth());
        System.out.println("Height: " + measurable.getHeight());
    }
}
