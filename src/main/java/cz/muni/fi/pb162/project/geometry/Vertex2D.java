package cz.muni.fi.pb162.project.geometry;

/**
* @author Vitalii Bortsov
*/

public class Vertex2D {

    private final double xcoord;
    private final double ycoord;

    /**
     *
     * @param xcoord coordinate
     * @param ycoord coordinate
     */
    public Vertex2D(double xcoord, double ycoord) {
        this.xcoord = xcoord;
        this.ycoord = ycoord;
    }

    public double getX() {
        return xcoord;
    }

    public double getY() {
        return ycoord;
    }

    /**
     *
     * @return info about vertex coordinates
     */
    public String toString() {
        return "[" + this.xcoord + ", " + this.ycoord + "]";
    }

    /**
     *
     * @param targetVertex must not be null
     * @return new Vertex in the middle point between targetVertex and this Vertex
     */
    public Vertex2D createMiddle(Vertex2D targetVertex) {
        double resultX = (this.xcoord + targetVertex.xcoord) / 2;
        double resultY = (this.ycoord + targetVertex.ycoord) / 2;
        return new Vertex2D(resultX, resultY);

    }

    /**
     *
     * @param targetVertex the point to which you want to know the distance
     * @return Euclidean distance between this vertex and input one. If input vertex is null, returns -1.0
     */
    public double distance(Vertex2D targetVertex) {
        if(targetVertex == null) {
            return -1.0;
        }
        return Math.sqrt(Math.pow((this.xcoord - targetVertex.xcoord), 2)
                + Math.pow((this.ycoord - targetVertex.ycoord), 2));
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof Vertex2D) {
            Vertex2D secondVertex = (Vertex2D) object;
            return (this.xcoord == secondVertex.xcoord) && (this.ycoord == secondVertex.ycoord);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Double.hashCode(xcoord) * 31 + Double.hashCode(ycoord);
    }
}
