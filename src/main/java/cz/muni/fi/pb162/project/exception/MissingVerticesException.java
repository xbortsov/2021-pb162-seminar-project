package cz.muni.fi.pb162.project.exception;

/**
 * Exception for objects with missing vertices
 * @author Vitalii Bortsov
 */
public class MissingVerticesException extends RuntimeException {
    /**
     * Exception with message
     * @param errorMessage message for an exception
     */
    public MissingVerticesException(String errorMessage) {
        super(errorMessage);
    }

    /**
     * Exception with message and error
     * @param errorMessage message for an exception
     * @param err error
     */
    public MissingVerticesException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }
}
