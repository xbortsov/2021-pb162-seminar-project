package cz.muni.fi.pb162.project.geometry;

import cz.muni.fi.pb162.project.utils.SimpleMath;

/**
 * Class for creating a triangle from 3 vertices
 * @author Vitalii Bortsov
 */
public class Triangle extends ArrayPolygon implements Measurable {
    private final Triangle[] arrayOfTriangles = {null, null, null};

    /**
     *
     * @param v1 1st vertex in new triangle
     * @param v2 2nd vertex in new triangle
     * @param v3 3rd vertex in new triangle
     */
    public Triangle(Vertex2D v1, Vertex2D v2, Vertex2D v3) {
        super(new Vertex2D[] {v1, v2, v3});
    }

    /**
     * 4-parametric constructor with depth for overloaded divide()
     * @param v1 1st vertex in new triangle
     * @param v2 2nd vertex in new triangle
     * @param v3 3rd vertex in new triangle
     * @param depth if value < 1 then the division will not occur
     */
    public Triangle(Vertex2D v1, Vertex2D v2, Vertex2D v3, int depth) {
        this(v1, v2, v3);
        this.divide(depth);
    }

    /**
     *
     * @return string with info about vertices in triangle
     */
    @Override
    public String toString() {
        return "Triangle: vertices=" + getVertex(0) + " " + getVertex(1) + " " +  getVertex(2);
    }

    /**
     *
     * @return true if triangle has been already divided
     */
    public boolean isDivided() {
        return arrayOfTriangles[0] != null;
    }

    /**
     * divide the triangle if it is not divided
     * @return true if triangle is not divided
     */
    public boolean divide() {
        if(this.isDivided()) {
            return false;
        }
        for(int index = 0; index < 3; index++) {
            Vertex2D point1 = getVertex(index);
            Vertex2D point2 = getVertex(index).createMiddle(getVertex((index + 1) % 3));
            Vertex2D point3 = getVertex(index).createMiddle(getVertex((index + 2) % 3));
            Triangle triangle = new Triangle(point1, point2, point3);
            arrayOfTriangles[index] = triangle;
        }
        return true;
    }

    /**
     * getter of sub-triangles
     * @param index must be in range 0-2
     * @return sub-triangle on the position <index>
     */
    public Triangle getSubTriangle(int index) {
        if(!this.isDivided()){
            return null;
        }
        if(index > 2 || index < 0) {
            return null;
        }
        return arrayOfTriangles[index];
    }

    /**
     * Method that checks if this triangle is equilateral or not
     * @return true if the triangle is equilateral
     */
    public boolean isEquilateral() {
        double side1 = getVertex(0).distance(getVertex(1));
        double side2 = getVertex(1).distance(getVertex(2));
        double side3 = getVertex(0).distance(getVertex(2));
        return Math.abs(side1 - side2) < 0.001 &&
                Math.abs(side1 - side3) < 0.001 &&
                Math.abs(side2 - side3) < 0.001;
    }

    /**
     * Method that divides a triangle into sub-triangles
     * @param depth if value < 1 then the division will not occur
     */
    public void divide(int depth) {
        if (depth <= 0) {
            return;
        }
        if (!this.divide()) {
            for(int index = 0; index < 3; index++) {
                this.arrayOfTriangles[index].divide(depth);
            }
            return;
        }
        this.divide(depth - 1);
    }

    @Override
    public double getWidth() {
        return SimpleMath.maxX(this) - SimpleMath.minX(this);
    }

    @Override
    public double getHeight() {
        return SimpleMath.maxY(this) - SimpleMath.minY(this);
    }
}
