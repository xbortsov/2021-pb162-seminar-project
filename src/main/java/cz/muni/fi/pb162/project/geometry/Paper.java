package cz.muni.fi.pb162.project.geometry;


import cz.muni.fi.pb162.project.exception.EmptyDrawableException;
import cz.muni.fi.pb162.project.exception.MissingVerticesException;
import cz.muni.fi.pb162.project.exception.TransparentColorException;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.List;
import java.util.Collections;
import java.util.ArrayList;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * This class simulates paper on which colored polygons can be drawn.
 *
 * @author Vitalii Bortsov
 */
public class Paper implements Drawable, PolygonFactory {
    private Color color;
    private final Set<ColoredPolygon> polygons;

    /**
     * Constructor with default values. Default color is black.
     */
    public Paper() {
        color = Color.BLACK;
        polygons = new HashSet<>();
    }

    /**
     * Constructor take a parameter of type Drawable
     * and copies the collection of drawn polygons.
     *
     * @param drawable drawable object
     */
    public Paper(Drawable drawable) {
        color = Color.BLACK;
        this.polygons = new HashSet<>(drawable.getAllDrawnPolygons());
    }

    @Override
    public void changeColor(Color color) {
        this.color = color;
    }

    @Override
    public void drawPolygon(Polygon polygon) throws TransparentColorException {
        if(color == Color.WHITE) {
            throw new TransparentColorException("Can't draw if color is " + color);
        }
        polygons.add(new ColoredPolygon(polygon, color));
    }

    @Override
    public void erasePolygon(ColoredPolygon polygon) {
        polygons.remove(polygon);
    }

    @Override
    public void eraseAll() throws EmptyDrawableException {
        if(this.polygons.size() == 0) {
            throw new EmptyDrawableException("This paper is empty!");
        }
        polygons.clear();
    }

    @Override
    public Collection<ColoredPolygon> getAllDrawnPolygons() {
        return Collections.unmodifiableSet(polygons);
    }

    @Override
    public int uniqueVerticesAmount() {
        Set<Vertex2D> vertices = new HashSet<>();
        List<ColoredPolygon> colPolygons = new ArrayList<>(this.polygons);
        for (ColoredPolygon colPolygon : colPolygons) {
            vertices.addAll(Arrays.asList(colPolygon.getAllVertices()));
        }
        return vertices.size();
    }

    @Override
    public Polygon tryToCreatePolygon(List<Vertex2D> vertices) throws MissingVerticesException {
        if(vertices == null) {
            throw new NullPointerException("List is empty");
        }
        Polygon polygon;
        List<Vertex2D> copy = new ArrayList<>(vertices);
        try {
            polygon = new CollectionPolygon(copy);
        } catch (IllegalArgumentException e) {
            copy.removeIf(Objects::isNull);
            try {
                polygon = tryToCreatePolygon(copy);
            } catch (NullPointerException ex) {
                throw new MissingVerticesException("There is not enough not null vertices");
            }
        }
        return polygon;
    }

    @Override
    public void tryToDrawPolygons(List<List<Vertex2D>> collectionPolygons) throws EmptyDrawableException {
        Polygon polygon;
        int drawn = 0;
        Throwable err = null;
        for (List<Vertex2D> collectionPolygon : collectionPolygons) {
            try {
                polygon = tryToCreatePolygon(collectionPolygon);
            } catch (MissingVerticesException | NullPointerException e) {
                err = e;
                continue;
            }
            try {
                this.drawPolygon(polygon);
            } catch (TransparentColorException e) {
                err = e;
                this.changeColor(Color.BLACK);
                continue;
            }
            drawn++;
        }
        if (drawn == 0) {
            throw new EmptyDrawableException("Nothing has been drawn", err);
        }
    }

    /**
     * Method, which returns all polygons with input color
     * @param color input color
     * @return all polygons with input color
     */
    public Collection<Polygon> getPolygonsWithColor(Color color) {
        List<ColoredPolygon> copy = new ArrayList<>(polygons);
        return copy.stream()
                .filter(polygon -> polygon.getColor() == color)
                .map(ColoredPolygon::getPolygon)
                .collect(Collectors.toList());
    }
}
